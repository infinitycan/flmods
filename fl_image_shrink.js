// ==UserScript==
// @name FL images
// @match http://www.thefreshloaf.com/*
// ==/UserScript==
// 
// Return a "hash" of k
function h(k) {
  return k.src;
}

var styleFile = new RegExp('http://www.thefreshloaf.com/files/styles/.*');
var socialFile = new RegExp('http://www.thefreshloaf.com/sites/.*');
var amazonFile = new RegExp('http://images.amazon.com/.*');
// returns whether an image should be shrinkable
function shrinkable(k) {
  var s = k.src;
  if (!(styleFile.exec(s) || socialFile.exec(s) || amazonFile.exec(s))) {
    return true;
  } else {
    return false;
  }
}

var images = document.getElementsByTagName('img');
var shrunken = {}; // stores whether images are shrunken or not
var widths = {}; // stores original image widths
var shrunkenSize = 50; // size to resize to
var defaultUnshrunkenWidth = 500;

// set up for all shrinkable images
for (var i = 0; i < images.length; i++) {
  var img = images[i];
  if (shrinkable(img)) {
    // spotlight images don't have specified widths
    if (img.className == "spotlight") {
      widths[h(img)] = defaultUnshrunkenWidth;
    } else {
      widths[h(img)] = img.width;
    }
    shrunken[h(img)] = true;
    img.width = shrunkenSize;
    img.onclick=function(){
      if (shrunken[h(this)] == true) {
        this.width=widths[h(this)];
      } else {
        this.width=shrunkenSize;
      }
      shrunken[h(this)] = !shrunken[h(this)];
    };
  }
}
